/**
 * Created by Lucyfurnice on 6/11/2016.
 */

var fs              = require('fs'),
    path            = require('path'),
    peerServer      = require('peer').ExpressPeerServer;
    //easyRTC         = require('easy-rtc');
    //holla         = require('holla');


function PortalRouteFactory(datastore, WSGIMiddleWareEngine) {
    this.WSGIMiddlewareEngine = WSGIMiddleWareEngine;
    this.datastore = datastore;
    this.db = this.datastore.orm;
    this.route = this.WSGIMiddlewareEngine.Router();

    this.route.get('/',function(req,res) {
        var orm = datastore;
        console.log(`req.datastore = ${req.datastore}`);
        res.send('rtc::route::portal:: Hello~!');
    });
};

PortalRouteFactory.prototype.initialize = function() {
    console.log('ext.rtc.PortalRouteFactory.initialize()');
};

exports = module.exports = PortalRouteFactory;