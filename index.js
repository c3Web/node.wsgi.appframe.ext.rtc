/**
 * Created by Lucyfurnice on 6/11/2016.
 */

'use strict';

var fs      = require('fs'),
    path    = require('path');


function WebRTCExtensionFactory(application, supercls) {
    this.superClass = supercls;
    this.duperClass = new this.superClass(this,application,__dirname);
    WebRTCExtensionFactory.super_.prototype.constructor.call(this, this, application, __dirname);
};

WebRTCExtensionFactory.prototype.initialize = function initialize() {
    WebRTCExtensionFactory.super_.prototype.initialize.call( this );
};

WebRTCExtensionFactory.prototype.findModels = function findModels() {
    WebRTCExtensionFactory.super_.prototype.findModels.call( this );
};

WebRTCExtensionFactory.prototype.findRoutes = function findRoutes ( ) {
    WebRTCExtensionFactory.super_.prototype.findRoutes.call( this );
};

module.exports = WebRTCExtensionFactory;